# ts-heap
Heap data-structure, written in typescript.

The package includes umd and bundles of amd and system.

# Install
`npm install ts-heap`

# Usage
Use the intellisense, or look at the tests.  
Supports removimg an item.