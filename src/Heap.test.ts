import { TestSuite, TestParams } from "just-test-api";
import { assert } from "chai";
import { Heap, minNumberCompare } from "./Heap";

function createRandomNumbersMinHeap(size: number = 0): Heap<number> {
	let heap = new Heap<number>(minNumberCompare);

	for (var i = 0; i < size; i++) {
		heap.add((Math.random() - 0.5) * (size * 10));
	}

	return heap;
}

export default function (suite: TestSuite) {
	suite.describe("Heap", () => {

		suite.describe("add", suit => {

			suite.test("Adding an item twice, throws.", (t) => {
				let heap = new Heap<number>(minNumberCompare);

				heap.add(1);

				assert.throws(() => heap.add(1));
			});
		});

		suite.describe("compare", suit => {

			suite.test("When compare return typeof different then number, throws.", (t) => {
				let heap = new Heap<number>(() => undefined!);
				heap.add(1); // First item will not call compare.

				assert.throws(() => heap.add(2));
			});
			
			suite.test("When compare return NaN, throws.", (t) => {
				let heap = new Heap<number>(() => NaN);
				heap.add(1); // First item will not call compare.

				assert.throws(() => heap.add(2));
			});
		});

		suite.describe("size", () => {

			suite.test("When has element after removal.", (t) => {
				let heap = createRandomNumbersMinHeap(4);
				assert.equal(heap.size, 4);
				heap.pop();
				assert.equal(heap.size, 3);
			});

			suite.test("When just created (empty).", (t) => {
				let heap = new Heap<number>(minNumberCompare);
				assert.equal(heap.size, 0);
			});

			suite.test("When empty after removal.", (t) => {
				let heap = createRandomNumbersMinHeap(2);
				heap.pop();
				heap.pop();
				assert.equal(heap.size, 0);
			});
		});

		suite.describe("pop()", suite => {

			suite.test("When empty heap, return undefined.", (t) => {
				let heap = new Heap<number>(minNumberCompare);
				assert.equal(heap.peek(), undefined);
			});

			suite.test("When heap runs empty(due to pop), return undefined.", (t) => {
				let heap = new Heap<number>(minNumberCompare);
				heap.add(5);
				heap.pop();
				assert.equal(heap.peek(), undefined);
			});

			suite.test("When min heap, return min value and remove element.", (t) => {
				let heap = new Heap<number>(minNumberCompare);
				heap.add(5);
				heap.add(3);
				heap.add(4);
				heap.add(2);
				heap.add(6);

				assert.equal(heap.pop(), 2);
				assert.equal(heap.pop(), 3);
				assert.equal(heap.pop(), 4);
				assert.equal(heap.pop(), 5);
				assert.equal(heap.pop(), 6);
				assert.equal(heap.pop(), undefined);
			});

		});

		suite.describe("peek()", suite => {

			suite.test("When min heap, return min value.", (t) => {
				let heap = new Heap<number>(minNumberCompare);
				heap.add(5);
				heap.add(3);
				heap.add(4);
				assert.equal(heap.peek(), 3);
			});

			suite.test("When min heap, after pops and adds, return min value.", (t) => {
				let heap = new Heap<number>(minNumberCompare);
				heap.add(5);
				assert.equal(heap.peek(), 5);
				heap.add(3);
				assert.equal(heap.peek(), 3);
				heap.add(4);
				assert.equal(heap.peek(), 3);
				heap.add(6);
				assert.equal(heap.peek(), 3);

				heap.pop();
				assert.equal(heap.peek(), 4);
				heap.pop();
				assert.equal(heap.peek(), 5);
				heap.pop();
				assert.equal(heap.peek(), 6);
			});

			suite.test("When empty heap, return undefined.", (t) => {
				let heap = new Heap<number>(minNumberCompare);
				assert.equal(heap.peek(), undefined);
			});

		});

		suite.describe("isEmpty", suite => {

			suite.test("test.", (t) => {
				let heap = new Heap<number>(minNumberCompare);
				assert.equal(heap.isEmpty, true);

				heap.add(5);
				assert.equal(heap.isEmpty, false);

				heap.add(3);
				heap.add(4);
				assert.equal(heap.isEmpty, false);

				heap.pop();
				assert.equal(heap.isEmpty, false);
				heap.pop();
				heap.pop();
				assert.equal(heap.isEmpty, true);
				heap.pop();
				assert.equal(heap.isEmpty, true);
			});
		});

		suite.describe(Heap.prototype.remove.name, suite => {

			suite.test("Removes the item when found.", test => {
				test.arrange();
				let heap = new Heap<number>(minNumberCompare);
				heap.add(5);
				heap.add(3);
				heap.add(4);
				assert.equal(heap.peek(), 3);

				test.act();
				heap.remove(3);

				test.assert();
				assert.equal(heap.peek(), 4);
				heap.remove(4);
				assert.equal(heap.peek(), 5);
			});

			suite.test("Removing last element.", test => {
				test.arrange();
				let heap = new Heap<number>(minNumberCompare);
				heap.add(4);

				test.act();
				heap.remove(4);
			});

			suite.test("Removing last element in the array when length > 1.", test => {
				test.arrange();
				let heap = new Heap<number>(minNumberCompare);
				heap.add(0);
				heap.add(1);
				heap.add(2);
				heap.add(3);

				test.act();
				heap.remove(3);
			});

			suite.test("Doesn't remove when not found.", (t) => {
				let heap = new Heap<number>(minNumberCompare);
				heap.add(5);
				heap.add(3);
				heap.add(4);
				assert.equal(heap.peek(), 3);

				heap.remove(10);
				assert.equal(heap.peek(), 3);
			});

			suite.test("Returns true when found.", (t) => {
				let heap = new Heap<number>(minNumberCompare);
				heap.add(3);
				let result = heap.remove(3);

				assert.isTrue(result);
			});

			suite.test("Returns false when not found.", (t) => {
				let heap = new Heap<number>(minNumberCompare);
				heap.add(10);
				let result = heap.remove(3);

				assert.isFalse(result);
			});
		});

		suite.describe("clear", suite => {

			suite.test("test.", (t) => {
				let heap = createRandomNumbersMinHeap(5);
				heap.clear();
				assert.equal(heap.isEmpty, true);
				assert.equal(heap.peek(), undefined);
			});
		});

		suite.describe("contains", suite => {

			suite.test("test.", (t) => {
				let heap = new Heap<number>(minNumberCompare);
				heap.add(1);
				heap.add(2);
				assert.equal(heap.contains(1), true);
				assert.equal(heap.contains(2), true);
				assert.equal(heap.contains(3), false);
			});
		});
	});
}